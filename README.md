# Nim interface to Portaudio C library. 

### _NOTE: This library has been tested on   WINDOWS only.Linux platform is not supported for now._

## Prerequisities
* A precompiled library named as  ``libportaudio.dll`` must be present in Window's PATH.

_NOTE: If you are unable to find one or build one a precompiled dll is present in ``lib/`` folder .Make sure to put it somewhere where executable can find it._ 


## Installation
* ```$ nimble install https://gitlab.com/eagledot/nim-portaudio```

## Usage:
```nim 
import portaudio
```

For more detailed usage see examples in ``examples/`` directory.


