# Package

version       = "0.1.0"
author        = "Anubhav (eagledot)"
description   = "PortAudio bindings for Nim"
license       = "MIT"
srcDir        = "src"


# Dependencies
requires "nim >= 1.0.0"

skipDirs = @["examples","lib"]
