#Simple example to record and simultaneously play audioData .
import os,portaudio
let
    stream: pointer = nil  
    userData : pointer = nil
    sampleRate: cdouble = 44100
    inpChannels = 1'i32  #set it to 0 then wouldnot open any input device
    outChannels = 1'i32  #set to it 0 .wouldnot open any output device.
    #nSeconds = 5  #no of Seconds to record the audio for.
    chunk = 512*2

#callBack function scheduled by OS whenver audio read/write data is available.
proc playbackCallback(inpBuff: pointer, outBuff: pointer,frameCount: culong,timeInfo: ptr PaStreamCallbackTimeInfo ,statusFlags: PaStreamCallbackFlags,userData: pointer): PaStreamCallbackResult {.cdecl.} =
    #expects inpChannels and outChannels are equal.
    copymem(outBuff,inpBuff,int(frameCount)*sizeof(int16)*outChannels)
    return paContinue

var code: PaError = initPortAudio()
echo($code," in Initializing the stream")
let
    devicesCount : PaDeviceIndex = getDeviceCount()
    defaultInput: PaDeviceIndex = getDefaultInputDevice()
    defaultOutput: PaDeviceIndex  = getDefaultOutputDevice()
echo("Count  of devices found: ", int(devicesCount))

#opening stream with default input and output devices ..and trying to get 16 bit PCm DATA with sampleRate.
#TODO:  Making sure that given sampleRate is supported by chosen inputDevice.
code  = defaultStream(unsafeAddr(stream),inpChannels,outChannels,paInt16,sampleRate,culong(chunk),playbackCallback ,userData);
echo(code," in opening the default stream")
#Now start the stream
code = startStream(stream)
echo(code," in starting the stream with default parameters")
echo("press ctrl-c to stop ... ")
while isStreamActive(stream) == 1'i32:
    sleep(100)

#code = stopStream(stream)
#echo(code," in stopping the stream")
#code = closeStream(stream)
#echo(code," in closing the stream")

#terminate ...finished using portaudio.
#code = terminatePortAudio();
#echo(code," in  terminating the stream")