
#Just a sample function to see more information about the devices on a particular machine.
import portaudio
#Initialize the portaudio
var code: PaError  = initPortAudio()
echo("Initializing: ",code)
let nDevices  = getDeviceCount()
echo("No of devices found: ",int(nDevices))
#list the more information about the devices just found..one reason is to allow user to interactively choose a particular device.
var  deviceInfo: ptr PaDeviceInfo

for ix in 0..int32(nDevices)-1 :
    deviceInfo  = getDeviceInfo(ix)
    echo("Device Index: ",int(ix)," Name: ",$(deviceInfo.name)," DefaultSamplerate: ",deviceInfo.defaultSampleRate)

echo("Default Input: ",int(getDefaultInputDevice()))
echo("Default Output: ",int(getDefaultOutputDevice()))


#terminate 
code = terminatePortAudio()
echo(code," TERMINATING")

