import  os
                             
const 
    DllName =
        when defined(windows):
            "libportaudio.dll"  #make sure this is in window's PATH

#define an enum for sampleFormat...recommended to use paInt16.
type
    Pa_sampleFormat* {.size: sizeof(culong).} = enum
        #define all the flags
        paFloat32    = 0x00000001
        paInt32  =      0x00000002
        paInt24   =        0x00000004   #packed 24 bit do not full definition about this ..
        paInt16   =        0x00000008
        paInt8     =       0x00000010
        paUInt8    =      0x00000020
        paCustomFormat = 0x00010000  #do not use for now.
        paNonInterleaved = 0x80000000  #do not know full definition about this 

type 
    PaDeviceIndex*  = cint
    PaTime* = cdouble

#Defining a structure to hold the information of devices pinged by portaudio.
type
    PaDeviceInfo* = object
        structVersion* : cint 
        name* : cstring
        hostApi*: cint 

        maxInputChannels* : cint
        maxOutputChannels* : cint

        defaultLowInputLatency*: PaTime
        defaultLowOutputLatency*: PaTime
        # Default latency values for interactive performance.
        defaultHighInputLatency*: PaTime
        defaultHighOutputLatency*: PaTime

        defaultSampleRate*: cdouble


#control the behaviour of stream..Actually these flags are passed to the CallBack ..
#We override the default behaviour 
type
    PaStreamFlags* {.size: sizeof(culong).} = enum
        paNoFlag = 0
        paCliffOff = 0x00000001 #disable default clipping out of range samples
        paDitheroff= 0x00000002 #disable default dithering 
        paNeverDropInput = 0x00000004 #Never dropout oveflowed samples does work only with automatic length of buffer ,do not use this generally.
        paPrimeOutputBuffersUsingStreamCallback = 0x00000008
        paPlatformSpecificFlags = 0xFFFF0000


#for more details see #538 in include/portaudio.h
#used with openStream procedure ...kind of defining options for a device.
type 
    PaStreamParameters = object
        device: PaDeviceIndex
        channelCount: cint
        sampleFormat: Pa_sampleFormat 
        suggestedLatency: PaTime
        #when in doubt leave this as nil.
        hostApiSpecificStreamInfo: pointer
type 
    PaError* = int32
   
    PaErrorCodes = enum
        paNotInitialized = -10000,
        paUnanticipatedHostError,
        paInvalidChannelCount,
        paInvalidSampleRate,
        paInvalidDevice,
        paInvalidFlag,
        paSampleFormatNotSupported,
        paBadIODeviceCombination,
        paInsufficientMemory,
        paBufferTooBig,
        paBufferTooSmall,
        paNullCallback,
        paBadStreamPtr,
        paTimedOut,
        paInternalError,
        paDeviceUnavailable,
        paIncompatibleHostApiSpecificStreamInfo,
        paStreamIsStopped,
        paStreamIsNotStopped,
        paInputOverflowed,
        paOutputUnderflowed,
        paHostApiNotFound,
        paInvalidHostApi,
        paCanNotReadFromACallbackStream,
        paCanNotWriteToACallbackStream,
        paCanNotReadFromAnOutputOnlyStream,
        paCanNotWriteToAnInputOnlyStream,
        paIncompatibleStreamHostApi,
        paBadBufferPtr,
        paNoError = 0,


proc toCString(errorCode: PaError): cstring {.importc: "Pa_GetErrorText", cdecl, dynlib: DllName.}
proc `$`*(errorCode: PaError): string = $(errorCode.toCstring())
#Initialize the portAudio..must be called only once and at top
proc initPortAudio*(): PaError {.importc: "Pa_Initialize",cdecl, dynlib: DllName.}
proc terminatePortAudio*() : PaError {.importc: "Pa_Terminate",cdecl,dynlib: DllName.}
type
    PaStreamCallbackFlags* {.size :sizeof(culong).} = enum
        paInputUnderflow = 0x00000001
        paInputOverflow = 0x00000002
        paOutputUnderflow = 0x00000004
        paOutputOverflow = 0x00000008
        paPrimingOutput = 0x00000010


type
    PaStreamCallbackTimeInfo* = object
        inputBufferAdcTime: PaTime  #/**< The time when the first sample of the input buffer was captured at the ADC input */
        currentTime: Patime         #/**< The time when the stream callback was invoked */
        outputBufferDacTime: Patime #/**< The time when the first sample of the output buffer will output the DAC */
type 
    PaStreamCallbackResult* {.size: sizeof(cint)} = enum
        paContinue = 0,   #/**< Signal that the stream should continue invoking the callback and processing audio. */
        paComplete = 1,    #/**< Signal that the stream should stop invoking the callback and finish once all output samples have played. */

        paAbort  = 2      #/**< Signal that the stream should stop invoking the callback and finish once all output samples have played. */
    
    #callBack type to be to be passed while opening Stream.
type
    AudioCallback* = proc(inpBuff: pointer, out_buffer: pointer,frameCount: culong,timeInfo: ptr PaStreamCallbackTimeInfo ,
                        statusFlags: PaStreamCallbackFlags,userData: pointer): PaStreamCallbackResult {.cdecl.}





#This is used for using  non-default input/output devices,supposed to provide option for that device...if in doubt use defaultStream. 
proc openStream(stream_ptr: ptr pointer,inputOptions: PaStreamParameters,outputOptions: PaStreamParameters , sampleRate: cdouble,framesPerBuffer: culong, flags: PaStreamFlags,callback_func: AudioCallback,
        userData: pointer): PaError {.importc: "Pa_openStream",cdecl,dynlib: DllName.}


#provide the number of input and output Channels,sampleRate,number of samples u want to read.
proc defaultStream*(stream_ptr :ptr pointer,n_inp: cint,n_out: cint,format: Pa_sampleFormat,sampleRate: cdouble,framesPerBuffer: culong,
    callback_func: AudioCallback,userData: pointer): PaError {.importc: "Pa_OpenDefaultStream",cdecl,dynlib: DllName.}



proc startStream*(stream: pointer): PaError {.importc : "Pa_StartStream",cdecl,dynlib: DllName.}
#closes the stream should be called after stream becomes inactive..i.e by returning paComplete or paAbort
proc closeStream*(stream: pointer): PaError {.importc: "Pa_CloseStream",cdecl,dynlib: DllName.}
#manually stop the stream..say after some seconds..controlled from main thread.
proc stopStream*(stream: pointer): PaError {.importc: "Pa_StopStream",cdecl,dynlib: DllName.}
#forcefully abort the stream ..either use stopstream or abortstream.
proc abortStream*(stream: pointer): PaError {.importc: "Pa_AbortStream",cdecl,dynlib: DllName.}

#return Returns one (1) when the stream is active (ie playing or recording
 #audio), zero (0) when not playing or, a PaError codes (which are always negative)
 #if PortAudio is not initialized or an error is encountered.
proc isStreamActive*(stream: pointer): PaError {.importc: "Pa_IsStreamActive",cdecl,dynlib: DllName.}
proc isStreamStopped*(stream: pointer): PaError {.importc: "Pa_IsStreamStopped",cdecl,dynlib: DllName.}



proc getDeviceCount*(): PaDeviceIndex {.importc: "Pa_GetDeviceCount" ,cdecl,dynlib: DllName.}
proc getDefaultInputDevice*(): PaDeviceIndex {.importc: "Pa_GetDefaultInputDevice" ,cdecl,dynlib: DllName.}
proc getDefaultOutputDevice*(): PaDeviceIndex {.importc: "Pa_GetDefaultOutputDevice" ,cdecl,dynlib: DllName.}
proc getDeviceInfo*(ix: PaDeviceIndex): ptr PaDeviceInfo {.importc: "Pa_GetDeviceInfo",cdecl,dynlib: DllName.}


when isMainModule:
    proc getVersion(): cstring {.importc: "Pa_GetVersionText",cdecl,dynlib: DllName.}
    echo($getVersion())